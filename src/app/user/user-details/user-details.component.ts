import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent {
  @Input() user: User;
  @Output() deleteUserHandler = new EventEmitter();

  constructor(private userService: UserService) { }

  delete(userId: number): void {
    this.userService.delete(userId)
      .then(users => {
        this.deleteUserHandler.emit();
      });
  }
}
