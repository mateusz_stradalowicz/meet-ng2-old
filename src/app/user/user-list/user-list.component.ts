import { Component, OnInit } from '@angular/core';
import * as Enumerable from 'linq';
import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[];
  selectedUser: User;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers().then(users => this.users = users);
  }

  selectUser(user: User) {
    if (this.selectedUser && user.id === this.selectedUser.id) {
      this.selectedUser = undefined;
    } else {
      this.selectedUser = user;
    }
  }

  handleDeleteUser() {
    this.selectedUser = undefined;
  }
}
