import { Routes } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { UserListComponent } from '../user/user-list/user-list.component';
import { BookListComponent } from '../book/book-list/book-list.component';
import { BookDetailsComponent } from '../book/book-details/book-details.component';

export const appRoutes: Routes = [
  { path: 'users', component: UserListComponent },
  { path: 'books', component: BookListComponent },
  { path: 'book/:id', component: BookDetailsComponent },
  { path: '', redirectTo: '/users', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];
