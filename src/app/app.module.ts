import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryBooksService } from './book/book.mock';

import { UserModule } from './user/user.module';
import { BookModule } from './book/book.module';
import { CoreModule } from './core/core.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    UserModule,
    BookModule,
    CoreModule,
    InMemoryWebApiModule.forRoot(InMemoryBooksService, { delay: 1500 })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
