import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Book } from './book';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BookService {

  constructor(private http: Http) { }

  getBooks(): Promise<Book[]> {
    return this.http.get('api/books')
      .toPromise()
      .then(response => response.json().data);
  }

  getBook(id: number) {
    return this.http.get('api/books/' + id);
  }

  updateBook(book: Book) {
    return this.http
      .put('api/books/' + book.id, JSON.stringify(book))
      .toPromise()
      .then(() => book);
  }
}
