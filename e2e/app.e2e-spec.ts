import { MeetNg2Page } from './app.po';

describe('meet-ng2 App', () => {
  let page: MeetNg2Page;

  beforeEach(() => {
    page = new MeetNg2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
